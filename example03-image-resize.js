/**
 * This program is NOT a web server. It is a simple program which will process image files using a combination of
 * the "fs" module and the "Jimp" image manipulation library. It will create thumbnails of all the images in a
 * given folder. The code here will be useful for Lab Exerise Four.
 */

// required libraries
var fs = require("fs");
var jimp = require("jimp");

// Config values
var inputFolder = __dirname + "/example03/fullsize/";
var outputFolder = __dirname + "/example03/thumbnails/";
var thumbWidth = 400;
var thumbHeight = 400;

// Scan all files in the inputFolder
fs.readdir(inputFolder, function (err, files) {

    console.log(files.length + " files found in " + inputFolder);

    // When the scan is complete, "files" will be an array of file names.
    // For each file...
    for (var i = 0; i < files.length; i++) {

        // ... If that file is an image...
        var file = files[i].toLowerCase();
        if (file.endsWith(".png") || file.endsWith(".bmp") ||
            file.endsWith(".jpg") || file.endsWith(".jpeg")) {

            // Generate the thumbnail
            var inputFileName = inputFolder + file;
            var outputFileName = outputFolder + file;
            generateThumbnail(inputFileName, outputFileName);
        }

    }

});

// This function will (asynchronously) generate a thumbnail for the given input file, and save it to the given output file.
function generateThumbnail(inputFileName, outputFileName) {
    console.log("Reading " + inputFileName + "...");
    jimp.read(inputFileName, function (err, image) {

        // When the image is loaded in Jimp, scale the image to fit
        // the desired size and then save it to the output folder.
        image
            .scaleToFit(thumbWidth, thumbHeight)
            .write(outputFileName, function (err) {
                // When the image is done saving, print this message to the console
                if (err) {
                    console.error("Error saving file " + outputFileName + "!");
                } else {
                    console.log(outputFileName + " saved successfully!");
                }
            });

    })
}